<?php get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>

      <div class="column">
        <h1 class="title-single"><?php post_type_archive_title(); ?></h1>
        
        <div class="filter">

        <?php
        $category_parent = get_category_by_slug('educational-materials');
        $categories = get_categories(array('child_of' => $category_parent->term_id));



        // FILTER
        echo '<input type="radio" id="tag-all" class="filter-tag" name="filter-radio" hidden="" checked="">';
        foreach ( $categories as $category ) { ?>

          <input type="radio" id="tag-<?php echo $category->slug; ?>" class="filter-tag" name="filter-radio" hidden="">

        <?php }

        // FILTER NAV
        echo '<div class="filter-nav">';
        echo '<label class="chip" for="tag-all">All</label>';
        foreach ( $categories as $category ) { ?>

          <label class="chip" for="tag-<?php echo $category->slug; ?>"><?php echo $category->name; ?></label>

        <?php }
        echo '</div>';

        echo '<div class="filter-body columns mb-4 mt-4">';
        foreach ( $categories as $category ) { ?>

          <?php
            $list_posts = get_posts(array(
              'post_type' => 'education',
              'posts_per_page' => -1,
              'post_status' => 'publish',
              'category_name' => $category->name
            ));

            if( $list_posts ):
              foreach( $list_posts as $post ): 
                setup_postdata( $post ); 
          ?>
           
            <div class="column col-12 pt-0 pb-0" data-tag="tag-<?php echo $category->slug; ?>">

              <div class="card mb-4">
                <div class="card-header">
                  <h4 class="card-title mb-1"><?php the_title(); ?></h4>

                  <div style="height:30px; padding-bottom:2em; margin-top:-1em">
                     <ul style="list-style-type:none; text-align:right">
                       <li style="display:inline"><div class="fb-share-button" data-layout="button" style="display:inline"></div>
                       <li style="display:inline"><div style="display:inline; position:relative; top:5px"><a href="https://twitter.com/intent/tweet" class="twitter-share-button" data-show-count="false">Tweet</a></div>
                       <li style="display:inline"><script type="IN/Share"></script>
                     </ul>
                   </div>

                </div>
                <div class="card-body"><?php the_excerpt(); ?></div>
                <div class="card-footer">
                  <?php if(get_field('download')) { ?>
                    <a href="<?php the_field('download') ?>">
                      <button class="btn btn-primary">Download</button>
                    </a>
                  <?php } ?>
                  <a href="<?php the_permalink(); ?>">
                    <button class="btn btn-primary">Read More</button>
                  </a>
                </div>
              </div>

            </div>

          <?php
            endforeach;
            wp_reset_postdata();
            endif;
            unset($list_posts);
          ?>

        <?php } // END - foreach $categories ?>

        </div> <!-- END .columns -->

        </div> <!-- END .filter -->

      </div> <!-- END Column MAIN -->

    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>
