O <a href="https://interscity.org">projeto InterSCity</a> dispõe de bolsas de Doutorado, Treinamento Técnico e Pós-Doutorado para pesquisa científica avançada em tópicos relacionados a Cidades Inteligentes.

Candidatos devem possuir sólidos conhecimentos em sistemas de software e/ou ciência de dados no nível de graduação ou mestrado.

Temas para pesquisa:

<ul>
<li>Ciência de Dados para Mobilidade Urbana em Grandes Metrópoles
<li>Governança participativa e democracia digital em Cidades Inteligentes
<li>Privacidade e Segurança Geolocalizada em Cidades Inteligentes
</ul>


O projeto dispõe de bolsas de doutorado no valor padrão da CAPES (R$ 2,2 mil), Bolsas de Pós-Doutorado (da CAPES de R$ 4,1 mil e da FAPESP de R$ 7.373,10) e Bolsas de Treinamento Técnico (R$ 3.104,80 para candidatos com 2 anos de formado, R$ 5.087,20 para 4 anos de formado e R$ 7.372,40 para 5 anos de formado ou doutorado).

Espera-se dos candidatos capacidade para trabalhar de forma independente e pró-ativa e que eles se engajem nas pesquisas do grupo de Sistemas de Software do IME-USP, interagindo e colaborando diariamente com os demais alunos, pesquisadores e professores do grupo de pesquisa. Durante a execução da pesquisa, o bolsista deverá residir no Estado de São Paulo e, após a reabertura dos trabalhos presenciais, deverá residir na cidade de São Paulo e frequentar o IME-USP diariamente.

Candidatos interessados devem preencher o formulário de manifestação de interesse em https://interscity.org/jobs/ o quanto antes, preferencialmente antes de 15 de fevereiro.
