<?php get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>
      
      <!-- Column MAIN -->
      <div class="column col-9 col-xs-12">
        <div class="columns">
          
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
          <?php
            $date = get_field('product_date', false, false);
            $date = new DateTime($date);
          ?>
          
          <div class="column col-8">
            <h1 class="title-single"><?php the_title(); ?></h1>

              <div style="height:30px; padding-bottom:2em; margin-top:-1em">
                <ul style="list-style-type:none; text-align:right">
                  <li style="display:inline"><div class="fb-share-button" data-layout="button" style="display:inline"></div>
		  <li style="display:inline"><div style="display:inline; position:relative; top:5px"><a href="https://twitter.com/intent/tweet" class="twitter-share-button" data-show-count="false">Tweet</a></div>
                  <li style="display:inline"><script type="IN/Share"></script>
                </ul>
              </div>

            <?php the_content(); ?>
          </div>
          
          <div class="column col-4">
            <?php if(get_field('product_name')) { ?>
              <div class="card card-mini">
                <div class="card-header">
                  <h4 class="title-mini">Product</h4>
                </div>
                <div class="card-body">
                  <h6 class="mb-0"><?php the_field('product_name'); ?></h6>
                  <p><?php echo $date->format('j M Y'); ?></p>
                  <a href="<?php the_field('product_link'); ?>" target="_blank"><button class="btn btn-primary">Download</button></a>
                </div>
              </div>
            <?php } ?>
            <?php if(get_field('documentation')) { ?>
              <div class="card card-mini">
                <div class="card-header">
                  <h4 class="title-mini">Resources</h4>
                </div>
                <div class="card-body">
                  <?php the_field('documentation'); ?>
                </div>
              </div>
            <?php } ?>
          </div>

        <?php endwhile; ?>

        <?php else: ?>
          <h3>Sorry, nothing to display.</h3>
        <?php endif; ?>
        
      </div> <!-- END Column MAIN -->

    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>
