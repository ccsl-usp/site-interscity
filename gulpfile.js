var
gulp = require('gulp'),
less = require('gulp-less'),
path = require('path'),
watch = require('gulp-watch');

//---

gulp.task('serve', ['less'], function() {
  gulp.watch(["sources/stylesheets/**/*.less"], ['less']);
});

gulp.task('default', ['serve']);
 
gulp.task('less', function () {
  return gulp.src('sources/stylesheets/main.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('assets/css'));
});