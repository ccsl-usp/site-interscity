<?php get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>

      <div class="column">
        <h1 class="title-single"><?php post_type_archive_title(); ?></h1>
        <?php $post_count = 1; $total = count($posts); ?>
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

          <div class="card mb-4">
            <div class="card-header">
              <h4 class="card-title mb-1"><?php the_title(); ?></h4>
            </div>
            <div class="card-body">
              <?php the_excerpt(); ?>
              <?php if(get_field('team')) { ?>
                <div class="card-team">
                  <h4 class="title-mini mb-1">Team</h4>
                  <?php the_field('team') ?>
                </div>
              <?php } ?>
            </div>
            <div class="card-footer pt-1">
              <a href="<?php the_permalink(); ?>">
                <button class="btn btn-primary">Read More</button>
              </a>
            </div>
          </div>

        <?php endwhile; ?>

        <?php else: ?>

          <h2>Sorry, nothing to display.</h2>

        <?php endif; ?>
        </div>

        <?php get_template_part('parts/pagination'); ?>

      </div> <!-- END Column MAIN -->

    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>