<?php
/* Template Name: Home */
get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>
      
      <!-- Column MAIN -->
      <div class="column">
        <div class="box-primary">
	  <p>The Future Internet will integrate large-scale systems constructed from the composition of thousands of distributed services, while interacting directly with the physical world via sensors and actuators, which compose the Internet of Things.</p>
          <p>InterSCity researchers seek to apply novel Computer Science and Technology techniques to tackle urban social problems in underprivileged neighborhoods and low-income populations, leveraging existing data and collecting and analyzing new datasets to support Evidence-based Public Policymaking.</p>
	</div>

        <!-- ROW 1 -->
        <div class="columns">
          <div class="column col-6 col-sm-12">
            <div class="card card-mini">
              <div class="card-header">
                <h4 class="title-mini">Recent Publications</h4>
                <?php get_publications__home(5); ?>
                <a href="/publications">See more publications</a>
              </div>
            </div>
          </div>

          <div class="column col-6 col-sm-12">
            <div class="card card-mini">
	          <div class="card-header">
                <h4 class="title-mini">Software</h4>
                <?php get_software__home(5); ?>
                <a href="/software">See more software</a>
              </div>
            </div>
          </div>
        </div>

        <hr class="separator">

        <!-- ROW 2 -->
        <div class="columns">
	      <div class="column col-6 col-sm-12">

          <?php
          date_default_timezone_set('America/Sao_Paulo');

          $list_posts = get_posts(array(
            'post_type' => 'events',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'orderby'     => 'meta_value',
            'order'       => 'ASC',
            'meta_key'      => 'event_date',
            'meta_query' => array(
              array(
                'key'   => 'event_date',
                'value'   => date('Ymd'),
                'compare'   => '>='
              )
            )
          ));

          if( $list_posts ): ?>

            <div class="card card-mini">
              <div class="card-header">
                <h4 class="title-mini">Next Events</h4>
              </div>
              <?php get_events__home(); ?>
	    </div>

          <?php
          ;
          endif;
          ?>

            <div class="card card-mini">
              <div class="card-header">
                <h4 class="title-mini">News</h4>
              </div>
              <?php get_news__home(2); ?>
              <a href="/news">See more news</a>
            </div>
            <!-- <div class="card card-mini">
              <div class="card-header">
                <h4 class="title-mini">Last Newsletter</h4>
              </div>
              <?php get_newsletters__home(5); ?>
            </div> -->
        </div>
	  <div class="column col-6 col-sm-12">
            <div class="card card-mini">
              <div class="card-header">
                <h4 class="title-mini">Recent press</h4>
              </div>
              <?php get_press__home(2); ?>
              <a href="/press">See more press reports</a>
            </div>
          </div>
        </div>

      </div> <!-- END Column MAIN -->
    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>
