<?php get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>
      
      <!-- Column MAIN -->
      <div class="column">
          
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
          <h1 class="title-single"><?php the_title(); ?></h1>
          <div class="columns">
            <div class="column col-10">

              <div style="height:30px; padding-bottom:4em; margin-top:-2em">
                <ul style="list-style-type:none; text-align:right">
                  <li style="display:inline"><div class="fb-share-button" data-layout="button" style="display:inline"></div>
		  <li style="display:inline"><div style="display:inline; position:relative; top:5px"><a href="https://twitter.com/intent/tweet" class="twitter-share-button" data-show-count="false">Tweet</a></div>
                  <li style="display:inline"><script type="IN/Share"></script>
                </ul>
              </div>

              <?php the_content(); ?>
            </div>
          </div>
          
        <?php endwhile; ?>

        <?php else: ?>
          <h3>Sorry, nothing to display.</h3>
        <?php endif; ?>
        
      </div> <!-- END Column MAIN -->

    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>
