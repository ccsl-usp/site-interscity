<?php get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>

      <div class="column">
        <h1 class="title-single"><?php post_type_archive_title(); ?></h1>

        <?php
          date_default_timezone_set('America/Sao_Paulo');
          
          $list_posts = get_posts(array(
            'post_type' => 'events',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'orderby'     => 'meta_value',
            'order'       => 'ASC',
            'meta_key'      => 'event_date',
            'meta_query' => array(
              array(
                'key'   => 'event_date',
                'value'   => date('Ymd'),
                'compare'   => '>='
              )
            )
          ));

          if( $list_posts ):
            foreach( $list_posts as $post ): 
              setup_postdata( $post );
        ?>

          <div class="card mb-4">
            <div class="card-header">
              <h4 class="card-title mb-1"><?php the_title(); ?></h4>
	      <h6 class="card-subtitle"><?php the_date_format(get_field('event_date')); ?><?php if(get_field('event_hour')) { echo ' | ' . get_field('event_hour'); } ?> | <?php the_field('place'); ?></h6>
              <div style="height:30px; padding-bottom:4em; margin-top:-2em">
                <ul style="list-style-type:none; text-align:right">
                  <li style="display:inline"><div class="fb-share-button" data-layout="button" data-href="<?php the_permalink(); ?>" style="display:inline"></div>
                  <li style="display:inline"><div style="display:inline; position:relative; top:5px"><a href="https://twitter.com/intent/tweet" class="twitter-share-button" data-show-count="false" data-url="<?php the_permalink(); ?>" data-text="InterSCity event | <?php the_title(); ?>">Tweet</a></div>
                  <li style="display:inline"><script type="IN/Share" data-url="<?php the_permalink(); ?>"></script>
                </ul>
              </div>
            </div>
            <div class="card-body"><?php the_excerpt(); ?></div>
            <div class="card-footer">
              <a href="<?php the_permalink(); ?>">
                <button class="btn btn-primary">Read More</button>
              </a>
            </div>
          </div>

        <?php
            endforeach;
            wp_reset_postdata();
            endif;
        ?>

        <h4 class="mt-4">Past Events</h4>

        <?php
          date_default_timezone_set('America/Sao_Paulo');
          
          $list_posts = get_posts(array(
            'post_type' => 'events',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'orderby'     => 'meta_value',
            'order'       => 'DESC',
            'meta_key'      => 'event_date',
            'meta_query' => array(
              array(
                'key'   => 'event_date',
                'value'   => date('Ymd'),
                'compare'   => '<'
              )
            )
          ));

          if( $list_posts ):
            foreach( $list_posts as $post ): 
              setup_postdata( $post );
        ?>

          <div class="card mb-4">
            <div class="card-header">
              <h4 class="card-title mb-1"><?php the_title(); ?></h4>
              <h6 class="card-subtitle"><?php the_date_format(get_field('event_date')); ?><?php if(get_field('event_hour')) { echo ' | ' . get_field('event_hour'); } ?> | <?php the_field('place'); ?></h6>
            </div>
            <div class="card-body"><?php the_excerpt(); ?></div>
            <div class="card-footer">
              <a href="<?php the_permalink(); ?>">
                <button class="btn btn-primary">Read More</button>
              </a>
            </div>
          </div>

        <?php
            endforeach;
            wp_reset_postdata();
            endif;
        ?>

      </div> <!-- END Column MAIN -->

    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>
