<?php
/* Template Name: BibTeX file */

header("Content-Type: text/x-bibtex");
header('Content-Disposition: attachment; filename="interscity.bib"');

    global $post;

    $list_posts = get_posts(array(
      'post_type'     => 'publications',
      'posts_per_page'  => -1,
      'post_status' => 'publish',
      )
    );

    if( $list_posts ):
      foreach( $list_posts as $post ): 
        setup_postdata( $post );
        print (wp_strip_all_tags (get_field('bibtex', false, false)));
?>


<?php
      endforeach;
      wp_reset_postdata();
    endif;

?>
