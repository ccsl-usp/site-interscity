<?php get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>

      <div class="column">
        <h1 class="title-single"><?php post_type_archive_title(); ?></h1>

	<?php if (have_posts()): $postnum = 1; while (have_posts()) : the_post(); ?>
        <?php if ($postnum == 1) {
                  $open = "is-open";
                  $arrow = "icon-arrow-up";
                  $display = 'style="display: block"';
              } else {
                  $open = "";
                  $arrow = "";
                  $display = "";
              }
         ?>

          <div class="card card-list <?= $open ?>"><i class="icon icon-arrow-down <?= $arrow ?>"></i>
            <div class="card-header">
              <h4 class="card-title mb-0"><?php the_title(); ?></h4>
            </div>
            <div class="card-body" <?= $display ?>>
              <?php the_content(); ?>
            </div>
          </div>

        <?php $postnum++; endwhile; ?>

        <?php else: ?>

          <h2>Sorry, nothing to display.</h2>

        <?php endif; ?>

        <?php get_template_part('parts/pagination'); ?>

      </div> <!-- END Column MAIN -->

    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>
