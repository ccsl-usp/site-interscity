<?php
  function get_software($postsperpage, $column = NULL, $evenodd = NULL) {
    global $post;

    $list_posts = get_posts(array(
      'post_type'     => 'software',
      'posts_per_page'  => $postsperpage,
      'post_status' => 'publish',
      'order' => 'ASC'
      )
    );
    
    if($evenodd) {
      $post_number = 0;
    }

    if( $list_posts ):
      foreach( $list_posts as $post ):
        if($evenodd) {

          $post_number++;
          if ($post_number % 2 == 0 && $evenodd == 'odd') {
            continue;
          }
          if ($post_number % 2 == 1 && $evenodd == 'even') {
            continue;
          }

        }
        setup_postdata( $post );

        // Echo Post
?>

  <?php if($column) { ?><div class="column column-zero col-6 col-md-12"><?php } ?>
  
  <div class="card card-software card-mini">
    <?php if(get_the_post_thumbnail()) { ?>
      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <?php the_post_thumbnail('thumbnail', ['class' => 'logo-software', 'title' => '']); ?>
      </a>
    <?php } else { ?>
      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo/logo-template.png" alt="" class="logo-software">
      </a>
    <?php } ?>
    <div class="card-header">
      <h4 class="card-title"><?php the_title(); ?></h4>
    </div>
    <div class="card-body">
      <?php the_excerpt(); ?>
      <a href="<?php echo get_permalink(); ?>"><button class="btn btn-primary">View details</button></a>
    </div>
  </div>

  <?php if($column) { ?></div><!-- END - column software --><?php } ?>

<?php
      endforeach;
      wp_reset_postdata();
      endif;
  }
?>
