<?php

//\( Date Functions
//\( ––––––––––––––––––––––––––––––––––––––––––––––––––

function separateData($data) {
  $result = [
    "ano" => substr($data, 0, 4),
    "mes" => substr($data, 4, 2),
    "dia" => substr($data, -2),
    "semana" => date('l', strtotime($data))
  ];
  return $result;
}

function abbrMonth($mes) {
  switch($mes) {
    case "01":
      return "jan";
      break;
    case "02":
      return "fev";
      break;
    case "03":
      return "mar";
      break;
    case "04":
      return "abr";
      break;
    case "05":
      return "mai";
      break;
    case "06":
      return "jun";
      break;
    case "07":
      return "jul";
      break;
    case "08":
      return "ago";
      break;
    case "09":
      return "set";
      break;
    case "10":
      return "out";
      break;
    case "11":
      return "nov";
      break;
    case "12":
      return "dez";
      break;
  }
}

function fullMonth($mes) {
  switch($mes) {
    case "01":
      return "Janeiro";
      break;
    case "02":
      return "Fevereiro";
      break;
    case "03":
      return "Março";
      break;
    case "04":
      return "Abril";
      break;
    case "05":
      return "Maio";
      break;
    case "06":
      return "Junho";
      break;
    case "07":
      return "Julho";
      break;
    case "08":
      return "Agosto";
      break;
    case "09":
      return "Setembro";
      break;
    case "10":
      return "Outubro";
      break;
    case "11":
      return "Novembro";
      break;
    case "12":
      return "Dezembro";
      break;
  }
}

function getSemana($semana) {
  switch($semana) {
    case "Sunday":
      return "Domingo";
      break;
    case "Monday":
      return "Segunda";
      break;
    case "Tuesday":
      return "Terça";
      break;
    case "Wednesday":
      return "Quarta";
      break;
    case "Thursday":
      return "Quinta";
      break;
    case "Friday":
      return "Sexta";
      break;
    case "Saturday":
      return "Sábado";
      break;
  }
}

function getSemanaShort($semana) {
  switch($semana) {
    case "Sunday":
      return "dom";
      break;
    case "Monday":
      return "seg";
      break;
    case "Tuesday":
      return "ter";
      break;
    case "Wednesday":
      return "qua";
      break;
    case "Thursday":
      return "qui";
      break;
    case "Friday":
      return "sex";
      break;
    case "Saturday":
      return "sáb";
      break;
  }
}

function getCalendarData($data, $option) {
  $date = separateData($data);
  switch($option) {
    case "dia":
      echo $date["dia"];
      break;
    case "mes":
      echo abbrMonth($date["mes"]);
      break;
    case "mes_full":
      echo fullMonth($date["mes"]);
      break;
    case "mes_number":
      echo $date["mes"];
      break;
    case "ano":
      echo $date["ano"];
      break;
    case "semana":
      echo getSemana($date["semana"]);
      break;
    case "semana_short":
      echo getSemanaShort($date["semana"]);
      break;
  }
}

?>