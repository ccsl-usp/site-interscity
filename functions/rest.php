<?php
// [index]
// Events


//\( Events
//\( ––––––––––––––––––––––––––––––––––––––––––––––––––

function rest_events( WP_REST_Request $request ) {

  // $transient_key = 'get-programacao'; 
  // $data = get_transient( $transient_key ); 

  // if ( $data == '' ) { 

    date_default_timezone_set('America/Sao_Paulo');
    $args = array(
      'post_type' => 'events',
      'posts_per_page' => -1,
      'post_status' => 'publish',
      'orderby'     => 'meta_value',
      'order'       => 'ASC',
      'meta_key'      => 'event_date',
      'meta_query' => array(
        array(
          'key'   => 'event_date',
          'value'   => date('Ymd'),
          'compare'   => '>='
        )
      )
    );

    $query = new WP_Query( $args );

    $posts = $query->posts;

    foreach ($posts as $key => $post) {
      $posts[$key]->post_content = $posts[$key]->post_content;
      $posts[$key]->url = get_permalink($post->ID);
      $posts[$key]->acf = get_fields($post->ID);
      $dateTemp = new DateTime(get_field('event_date', $post->ID));
      $posts[$key]->event_date = $dateTemp;
      $posts[$key]->event_date_name = $dateTemp->format('j M Y');
      unset($dateTemp);
    }

    $data = $posts;

    wp_reset_query();

    // set_transient( $transient_key, $data, 60 * 60 * 8 );

  // }

  return $data;
}

add_action( 'rest_api_init', function () {
  register_rest_route( 'rest', '/events', array(
    'methods' => 'GET',
    'callback' => 'rest_events'
  ) );
} );


?>