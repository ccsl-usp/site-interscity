<?php
  function get_publications($postsperpage, $year = '2017') {
    global $post;

    $list_posts = get_posts(array(
      'post_type'     => 'publications',
      'posts_per_page'  => $postsperpage,
      'post_status' => 'publish',
      'year'  => $year,
      )
    );

    if( $list_posts ):
      foreach( $list_posts as $post ): 
        setup_postdata( $post );

        // Echo Post
?>

  <div class="card card-list card-publication">
    <i class="icon icon-arrow-down"></i>
    <div class="card-header">
      <p class="card-title">
          <?php if(get_field('author')) { ?><?php the_field('author'); ?>. <?php } ?>
          <?php
            if (get_field('pdf')) {
              $link = esc_url(get_field('pdf'));
            } elseif (get_field('publish_version')) {
              $link = esc_url(get_field('publish_version'));
            };
          ?>
          <?php if(isset($link)) { ?>
              <a href="<?php echo $link; ?>" target="_blank"><?php the_title(); ?></a>.
          <?php } else { ?>
              <?php the_title(); ?>.
          <?php } ?>
          <?php if(get_field('booktitle')) { ?> <em><?php the_field('booktitle'); ?></em><?php } ?>,
          <?php the_time('Y'); ?>.</p>
    </div>
    <div class="card-body">
      <hr class="separator mt-3 mb-1">

      <?php if(get_the_content()) { ?>
        <h4 class="title-mini">Abstract</h4>
        <?php the_content(); ?>
      <?php } ?>

      <?php if(get_field('note')) { ?>
        <h4 class="title-mini">Note</h4>
        <?php the_field('note') ?>
      <?php } ?>

      <?php if(get_field('url')) { ?>
      <h4 class="title-mini">External URL</h4>
      <a href="<?php the_field('url'); ?>" target="_blank">
        <?php the_field('url'); ?>
      </a>
      <?php } ?>

      <?php if(get_field('pdf')) { ?>
      <h4 class="title-mini">PDF</h4>
      <a href="<?php the_field('pdf'); ?>" target="_blank">
        <button class="btn btn-primary">Download</button>
      </a>
      <?php } ?>

      <?php if(get_field('slides')) { ?>
        <h4 class="title-mini">Slides</h4>
        <a href="<?php the_field('slides'); ?>" target="_blank">
          <button class="btn btn-primary">Download</button>
        </a>
      <?php } ?>

      <?php if(get_field('slides_file')) { ?>
        <h4 class="title-mini">Slides</h4>
        <a href="<?php echo get_field('slides_file')['url']; ?>" target="_blank">
          <button class="btn btn-primary">Download</button>
        </a>
      <?php } ?>

      <?php if(get_field('bibtex')) { ?>
        <h4 class="title-mini">BibTeX</h4>
        <div class="bibtex-field"><?php the_field('bibtex'); ?></div>
      <?php } ?>
    </div>
  </div>
<?php
      endforeach;
      wp_reset_postdata();
      endif;
  }
?>
