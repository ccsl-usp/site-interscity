<?php
  function get_news__home($postsperpage) {
    global $post;

    $list_posts = get_posts(array(
      'post_type'     => 'post',
      'posts_per_page'  => $postsperpage,
      'post_status' => 'publish'
      )
    );

    if( $list_posts ):
      foreach( $list_posts as $post ): 
        setup_postdata( $post );

        // Echo Post
?>

  <a href="<?php the_permalink(); ?>" class="-no-decoration" target="_blank">
    <h5 class="mb-1"><?php the_title(); ?></h5>
  </a>
  <?php the_excerpt(); ?>
  <a href="<?php echo get_permalink(); ?>"><button class="btn btn-primary">Read more</button></a>
  <hr class="mb-2 mt-2 separator">

<?php
      endforeach;
      wp_reset_postdata();
      endif;
  }
?>
