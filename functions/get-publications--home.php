<?php
  function get_publications__home($postsperpage) {
    global $post;

    $list_posts = get_posts(array(
      'post_type'     => 'publications',
      'posts_per_page'  => $postsperpage,
      'post_status' => 'publish',
      'meta_query' => array(
          array(
              'key' => 'highlight',
              'value' => true
          )
      )
    ));

    if( $list_posts ):
      foreach( $list_posts as $post ): 
        setup_postdata( $post );

        // Echo Post
?>
  <div>
    <?php
      if (get_field('pdf')) {
        $link = esc_url(get_field('pdf'));
      } elseif (get_field('publish_version')) {
        $link = esc_url(get_field('publish_version'));
      };
    ?>
    <h5 class="mb-1">
    <?php if(isset($link)) { ?>
        <a href="<?php echo $link; ?>" class="-no-decoration" target="_blank">
            <?php the_title(); ?></a>.
    <?php } else { ?>
        <?php the_title(); ?>.
    <?php } ?>
    </h5>
    <p><?php if(get_field('booktitle')) { ?><em><?php the_field('booktitle'); ?>.</em><?php } ?>
    <?php if(get_field('author')) { ?><?php the_field('author'); ?><?php } ?>, <?php the_time('Y'); ?>.</p>
  </div>
  <hr class="mb-2 mt-2 separator">

<?php
      endforeach;
      wp_reset_postdata();
      endif;
  }
?>
