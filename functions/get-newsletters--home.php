<?php
  function get_newsletters__home($postsperpage) {
    global $post;

    $list_posts = get_posts(array(
      'post_type'     => 'newsletters',
      'posts_per_page'  => $postsperpage,
      'post_status' => 'publish'
      )
    );

    if( $list_posts ):
      foreach( $list_posts as $post ): 
        setup_postdata( $post );

        // Echo Post
?>
  <a href="<?php echo get_permalink(); ?>" class="-no-decoration">
  <div class="card-button">
    <h5><?php the_title(); ?></h5>
  </div>
  </a>

<?php
      endforeach;
      wp_reset_postdata();
      endif;
  }
?>