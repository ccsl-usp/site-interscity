<?php

// SOFTWARE - RELEASE
function software_release_shortcode( $atts ) {
  // $a = shortcode_atts( array(
  //   'foo' => 'something',
  //   'bar' => 'something else',
  // ), $atts );
  $date = new DateTime($atts['date']);
  $html = '';

  $html .= '<h6 class="mb-0">';
  $html .= $atts['name'];
  $html .= '</h6><p>';
  $html .= $date->format('j M Y');
  $html .= '<br><a href="';
  $html .= $atts['link'];
  $html .= '" target="_blank">Download</a></p>';

  return $html;
}
add_shortcode( 'release', 'software_release_shortcode' );

// SEPARATOR
function separator_shortcode() {
  return '<hr class="separator mb-2 mt-2">';
}
add_shortcode( 'separator', 'separator_shortcode' );

?>