<?php
  function get_software__home($postsperpage, $column = NULL) {
    global $post;

    $list_posts = get_posts(array(
      'post_type'     => 'software',
      'posts_per_page'  => $postsperpage,
      'post_status' => 'publish',
      'order' => 'ASC'
      )
    );

    if( $list_posts ):
      foreach( $list_posts as $post ): 
        setup_postdata( $post );

        // Echo Post
?>

  <?php if($column) { ?><div class="column column-zero col-6 col-md-12"><?php } ?>

  <a href="<?php echo get_permalink(); ?>" class="-no-decoration" target="_blank">
    <h5 class="mb-1"><?php the_title(); ?>.</h5>
  </a>
  <div><?php the_excerpt(); ?></div>
  <hr class="mb-2 mt-2 separator">

  <?php if($column) { ?></div><!-- END - column software --><?php } ?>

<?php
      endforeach;
      wp_reset_postdata();
      endif;
  }
?>
