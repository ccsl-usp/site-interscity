<?php
  function get_press__home($postsperpage) {
    global $post;

    $list_posts = get_posts(array(
      'post_type'     => 'press',
      'posts_per_page'  => $postsperpage,
      'post_status' => 'publish'
      )
    );

    if( $list_posts ):
      foreach( $list_posts as $post ): 
        setup_postdata( $post );

        // Echo Post
?>

  <div class="card-body">
    <a href="<?php the_permalink(); ?>">
      <h5 class="title"><?php the_title(); ?></h5>
    </a>
    <h6 class="subtitle"><?php if(get_field('source')) { ?>on <a href="<?php the_field('link') ?>"><?php the_field('source') ?><?php } ?></a></h6>
    <?php the_excerpt(); ?>
  <hr class="mb-2 mt-2 separator">
  </div>
<?php
      endforeach;
      wp_reset_postdata();
      endif;
  }
?>
