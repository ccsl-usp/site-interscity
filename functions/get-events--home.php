<?php
  function get_events__home() {
    global $post;

    $list_posts = get_posts(array(
        'post_type'     => 'events',
        'posts_per_page'  => 1,
        'post_status' => 'publish',
        'orderby'     => 'meta_value',
        'order'       => 'ASC',
        'meta_key'      => 'event_date',
        'meta_query' => array(
          array(
            'key'   => 'event_date',
            'value'   => date('Ymd'),
            'compare'   => '>='
          )
        )
      )
    );

    if( $list_posts ):
      foreach( $list_posts as $post ): 
        setup_postdata( $post );

        // Echo Post
?>

  <div class="card-body">
    <a href="<?php the_permalink(); ?>"><h5 class="title is-uppercase mb-1"><?php the_title(); ?></h5></a>
    <h6 class="subtitle"><?php the_date_format(get_field('event_date')); ?><?php if(get_field('event_hour')) { echo ' | ' . get_field('event_hour'); } ?> | <?php the_field('place'); ?></h6>
    <?php the_excerpt(); ?>
    <hr class="separator-invisible"><a href="https://calendar.google.com/calendar/ical/me74pfoqgvr7rknn4cb3pdpc5o%40group.calendar.google.com/public/basic.ics">Add our calendar on Google Calendar</a>
  </div>
<?php
      endforeach;
      wp_reset_postdata();
      endif;
  }
?>
