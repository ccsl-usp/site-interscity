<?php get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>

      <div class="column">
        <h1 class="title-single"><?php post_type_archive_title(); ?></h1>

        <?php get_template_part('parts/loop'); ?>

        <?php get_template_part('parts/pagination'); ?>

      </div> <!-- END Column MAIN -->

    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>