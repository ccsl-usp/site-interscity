<div class="card card-mini">
  <div class="card-header">
    <h4 class="title-mini">Next Events</h4>
  </div>
  <div class="card-body">
    <h5 class="title">I ENCONTRO INCT</h5>
    <h6 class="subtitle">in Juquehy</h6>
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam eget ligula eu lectus lobortis condimentum. Aliquam nonummy auctor massa.</p>
    <hr class="separator-invisible"><a href="#">Add our calendar on Google Calendar</a>
  </div>
</div>
<div class="card card-mini">
  <div class="card-header">
    <h4 class="title-mini">Last Newsletter</h4>
  </div>
  <div class="card-body">
    <h5 class="title">19/04/2017</h5>
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam eget ligula eu lectus lobortis condimentum.</p>
  </div>
</div>
<div class="card card-mini">
  <div class="card-header">
    <h4 class="title-mini">Sign in newsletter</h4>
  </div>
  <div class="card-body">
    <div class="input-group">
      <label class="form-label" for="newsletter"></label>
      <input class="form-input" type="text" id="newsletter" placeholder="Email to sign in"/>
      <button class="btn btn-primary">Sign In</button>
    </div>
  </div>
</div>
<div class="card card-mini">
  <div class="card-header">
    <h4 class="title-mini">Latest tweets</h4>
  </div>
  <div class="card-body"><a class="twitter-timeline" data-height="480" href="https://twitter.com/TwitterDev">Tweets by TwitterDev</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
  </div>
</div>
<div class="card card-mini">
  <div class="card-header">
    <h4 class="title-mini">Recent press</h4>
  </div>
  <div class="card-body">
    <h5 class="title">Workshop on Juquehy</h5>
    <h6 class="subtitle">on Estadão</h6>
    <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Etiam eget ligula eu lectus lobortis condimentum.</p>
  </div>
</div>