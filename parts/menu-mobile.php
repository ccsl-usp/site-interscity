<ul class="list-menu mb-4 show-xs show-sm show-md mt-4">
  <h4 class="title-mini">Menu</h4>
  <div class="form-group">
    <select class="form-select" style="width: 100%;" onChange="window.location.href=this.value">
      <option selected disabled>Go to...</option>
      <option value="/">Home</option>
      <option value="/about">About Us</option>
      <option value="/publications">Publications</option>
      <option value="/software">Software</option>
      <option value="/events">Events</option>
      <option value="/news">News</option>
      <!-- <option value="/newsletters">Newsletters</option> -->
      <option value="/members">Members</option>
      <option value="/research_themes">Research Themes</option>
      <option value="/education">Educational Materials</option>
      <option value="/meetings">Meeting Notes</option>
      <option value="/sponsors">Sponsors</option>
      <option value="/press">Press</option>
    </select>
  </div>
</ul>
