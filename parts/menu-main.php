<div class="column col-3 col-md-4 column-menu hide-xs hide-sm hide-md">
  <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
  <!-- <ul class="list-menu mb-4">
    <li><a class="is-selected" href="index.html" title="Home">Home</a></li>
    <li><a href="about-us.html" title="About Us">About Us</a></li>
    <li><a href="publications.html" title="Publications">Publications</a></li>
    <li><a href="software.html" title="Software">Software</a></li>
    <li><a href="events.html" title="Events">Events</a></li>
    <li><a href="news.html" title="News">News</a></li>
    <li><a href="newsletters.html" title="Newsletters">Newsletters</a></li>
    <li><a href="members.html" title="Members">Members</a></li>
    <li><a href="research-themes.html" title="Research Themes">Research Themes</a></li>
    <li><a href="educational-materials.html" title="Educational Materials">Educational Materials</a></li>
    <li><a href="meeting-notes.html" title="Meeting Notes">Meeting Notes</a></li>
    <li><a href="sponsors.html" title="Sponsors">Sponsors</a></li>
    <li><a href="recent-press.html" title="Recent Press">Recent Press</a></li>
  </ul> -->

  <?php get_sidebar(); ?>

  <h4 class="title-mini mt-4">Sponsors</h4>
  <!-- Major sponsors -->
  <?php
    $list_posts = get_posts(array(
      'post_type' => 'sponsors',
      'posts_per_page' => -1,
      'post_status' => 'publish',
      'meta_key'      => 'type',
      'meta_query' => array(
        array(
          'key'   => 'type',
          'value'   => 'major',
          'compare'   => '='
        )
      )
    ));

    if( $list_posts ):
      foreach( $list_posts as $post ): 
        setup_postdata( $post ); 
  ?>

    <div class="sponsor-side"><img src="<?php the_field('logo') ?>" alt=""/></div>

  <?php
    endforeach;
    wp_reset_postdata();
    endif;
    unset($list_posts);
  ?>
  
</div> <!-- END Column MENU -->
