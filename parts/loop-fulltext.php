<?php if (have_posts()): while (have_posts()) : the_post(); ?>

  <div class="card mb-4">
    <div class="card-header">
      <h4 class="card-title mb-1"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
      <h6 class="card-subtitle"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></h6>
      <div style="height:30px; padding-bottom:4em; margin-top:-2em">
        <ul style="list-style-type:none; text-align:right">
          <li style="display:inline"><div class="fb-share-button" data-layout="button" data-href="<?php the_permalink(); ?>" style="display:inline"></div>
          <li style="display:inline"><div style="display:inline; position:relative; top:5px"><a href="https://twitter.com/intent/tweet" class="twitter-share-button" data-show-count="false" data-url="<?php the_permalink(); ?>" data-text="<?php the_title(); ?>">Tweet</a></div>
          <li style="display:inline"><script type="IN/Share" data-url="<?php the_permalink(); ?>"></script>
        </ul>
      </div>
    </div>
    <div class="card-body"><?php the_content(); ?></div>
  </div>

<?php endwhile; ?>

<?php else: ?>

  <h2>Sorry, nothing to display.</h2>

<?php endif; ?>
