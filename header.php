<!DOCTYPE html>
  <html>
    <head>
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-5816632-3"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
      
        gtag('config', 'UA-5816632-3');
      </script>
      <meta charset="UTF-8" />
      <meta name="robots" content="index, follow" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta name="author" content="http://andrechou.com" />
      <meta name="twitter:site" content="@ccslusp" />
      <meta property="og:image" content="http://interscity.org/wp-content/themes/inct/interscity_opengraph.png" />
      <title><?php wp_title(); ?></title>
      
      <?php wp_head(); ?>
      
      <!-- Fonts - Source Sans Pro & Nunito Sans -->
      <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:regular,bold,black|Source+Sans+Pro:400,400i,600,600i" rel="stylesheet">

      <!-- Styles-->
      <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css" />
      
      <!-- Scripts -->
      <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery-3.2.0.min.js" type="text/javascript"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/assets/js/vue.min.js" type="text/javascript"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/assets/js/vue-resource.min.js" type="text/javascript"></script>
      <script type="text/javascript" src="https://cdn.jsdelivr.net/lodash/4.17.4/lodash.min.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/assets/js/slick.min.js" type="text/javascript"></script>

    </head>

    <body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.3"></script>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<script src="https://platform.linkedin.com/in.js" type="text/javascript">lang: en_US</script>

      <!-- Header Home -->
      <?php if(is_page_template('page-home.php')) { ?>
        <header class="header-home-bg">
          <canvas class="canvas-connections"></canvas>
          <div class="cityscape"></div>
        </header>
        <div class="container grid-base">
          <header class="header-home">
            <div class="columns">
              <div class="column col-3 col-xs-12 col-sm-8 col-md-6 col-lg-6 column-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo/interscity.svg" alt="INCT Interscity Logo"></a></div>
              <div class="column hide-sm hide-xs" style="align-self: flex-end;">
                <h3>Enabling the Future Internet for Smart Cities</h3>
              </div>
            </div>
            <div class="columns">
              <div class="column col-7 col-sm-12 col-md-10 column-hero-home">
                <h3 class="show-sm show-xs">Enabling the Future Internet for Smart Cities</h3>
              </div>
            </div>
          </header>
        </div>

      <!-- Header Other Pages -->
      <?php } else { ?>
        <header class="header-home-bg header-small">
          <canvas class="canvas-connections"></canvas>
          <div class="cityscape"></div>
        </header>
        <div class="container grid-base">
          <!-- Header-->
          <header class="header-home header-small">
            <div class="columns">
              <div class="column col-3 col-xs-12 col-sm-8 col-md-6 col-lg-6 column-logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo/interscity.svg" alt="INCT Interscity Logo"/></a></div>
              <div class="column hide-sm hide-xs" style="align-self: flex-end;">
                <h3>Enabling the Future Internet for Smart Cities</h3>
              </div>
            </div>
            <div class="columns">
              <div class="column col-7 col-sm-12 col-md-10 column-hero-home">
                <h3 class="show-sm show-xs">Enabling the Future Internet for Smart Cities</h3>
              </div>
            </div>
          </header>
        </div>
      <?php } ?>
