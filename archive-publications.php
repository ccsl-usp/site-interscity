<?php get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>

      <div class="column">
        <h1 class="title-single"><?php post_type_archive_title(); ?></h1>
        <a href="/publications-bibtex" target="_blank"><h6>Download the interscity.bib BibTeX file</h6></a>

<?php $list_posts = get_posts(array(
      'post_type'     => 'publications',
      'posts_per_page'  => $postsperpage,
      'post_status' => 'publish',
      'year'  => 2023,
      )
    );
?>

<?php if ($list_posts) { ?>

        <h4 class="mt-4">2024</h4>
        <?php get_publications(-1, 2024); ?>
        <h4 class="mt-4">2023</h4>
        <?php get_publications(-1, 2023); ?>
        <h4 class="mt-4">2022</h4>
        <?php get_publications(-1, 2022); ?>
        <h4 class="mt-4">2021</h4>
        <?php get_publications(-1, 2021); ?>
        <h4 class="mt-4">2020</h4>
        <?php get_publications(-1, 2020); ?>
        <h4 class="mt-4">2019</h4>
        <?php get_publications(-1, 2019); ?>
        <h4 class="mt-4">2018</h4>
        <?php get_publications(-1, 2018); ?>
        <h4 class="mt-4">2017</h4>
        <?php get_publications(-1, 2017); ?>
        <h4 class="mt-4">2016</h4>
        <?php get_publications(-1, 2016); ?>

<?php } ?>

      </div> <!-- END Column MAIN -->

    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>
