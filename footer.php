    <footer class="footer-main">
      <div class="container grid-base">
        <div class="columns">
          <div class="column col-4 col-sm-12">
            <p><a href="https://creativecommons.org/licenses/by/4.0/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/cc-by.svg" style="width:60px"></a></p>
          </div>
          <div class="column col-8 col-sm-12">
            <p class="align-right-desktop font-minor"><a href="/contact" target="_blank">Contact </a>| <a href="/creative-commons">Creative Commons </a>| <a href="/logos" target="_blank">Press Kit </a>| <a href="/sitemap_index.xml" target="_blank">Sitemap </a>| <a href="<?php bloginfo('rss2_url'); ?>" target="_blank">RSS </a></p>
          </div>
        </div>
      </div>
    </footer>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/canvas-connections.js" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/assets/js/main.js" type="text/javascript"></script>
  </body>
</html>
