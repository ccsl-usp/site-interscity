<?php get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>

      <div class="column">
        <h1 class="title-single"><?php post_type_archive_title(); ?></h1>
        <div class="columns">

        <!-- Major sponsors -->
        <?php
          $list_posts = get_posts(array(
            'post_type' => 'sponsors',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'meta_key'      => 'type',
            'meta_query' => array(
              array(
                'key'   => 'type',
                'value'   => 'major',
                'compare'   => '='
              )
            )
          ));

          if( $list_posts ):
            foreach( $list_posts as $post ): 
              setup_postdata( $post ); 
        ?>

          <div class="column col-3 col-xs-6">
            <div class="sponsor"><img src="<?php the_field('logo') ?>" alt=""/></div>
          </div>

        <?php
          endforeach;
          wp_reset_postdata();
          endif;
          unset($list_posts);
        ?>
        </div>


        <!-- Minor sponsors -->
        <?php
          $list_posts = get_posts(array(
            'post_type' => 'sponsors',
            'posts_per_page' => -1,
            'post_status' => 'publish',
            'meta_key'      => 'type',
            'meta_query' => array(
              array(
                'key'   => 'type',
                'value'   => 'minor',
                'compare'   => '='
              )
            )
          ));

          if( $list_posts ):
            foreach( $list_posts as $post ): 
              setup_postdata( $post ); 
        ?>

          <div class="column col-2 col-xs-6">
            <div class="sponsor"><img src="<?php the_field('logo') ?>" alt=""/></div>
          </div>

        <?php
          endforeach;
          wp_reset_postdata();
          endif;
          unset($list_posts);
        ?>
        </div>

      </div> <!-- END Column MAIN -->

    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>