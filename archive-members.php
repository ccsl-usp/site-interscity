<?php get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>

      <div class="column">
        <h1 class="title-single"><?php post_type_archive_title(); ?></h1>
        
        <?php
        $category_id = get_category_by_slug('institutions')->term_id;
        $categories = get_categories(array('child_of' => $category_id, 'orderby' => 'name', 'order' => 'ASC'));

        foreach ( $categories as $category ){ ?>
          <div class="card mb-4 p-2">
            
            <h4><?php echo $category->name; ?></h4>
            
            <?php
              $list_posts = get_posts(array(
                'post_type' => 'members',
                'posts_per_page' => -1,
                'post_status' => 'publish',
		'category_name' => $category->name,
		'orderby' => 'title',
		'order' => 'ASC'
              ));

              if( $list_posts ):
                foreach( $list_posts as $post ): 
                  setup_postdata( $post ); 
            ?>
              <div class="member mb-2">
                <h4 class="title-mini mb-0"><?php the_title(); ?></h4>
                <?php 
                /* It seems array_filter might not be ideal here */
                $field_list = ['homepage', 'lattes', 'google_scholar'];
                $active_field_list = [];
                foreach ( $field_list as $field ):
                    if (get_field($field)):
                        $active_field_list[] = $field;
                    endif;
                endforeach;

                for ( $i = 0; $i < count($active_field_list); $i++ ):
                    switch ($active_field_list[$i]) {
                    case 'homepage':
                        ?> <a href="<?php the_field('homepage') ?>">Homepage</a> <?php ;
                        break;
                    case 'lattes':
                        ?> <a href="<?php the_field('lattes') ?>">Lattes</a> <?php ;
                        break;
                    case 'google_scholar':
                        ?> <a href="<?php the_field('google_scholar') ?>">Google Scholar</a> <?php ;
                        break;
		    }

                    if ($i +1 < count($active_field_list)):
                        ?> | <?php ;
                    endif;
                endfor;
                ?>
              </div>

            <?php
              endforeach;
              wp_reset_postdata();
              endif;
              unset($list_posts);
            ?>

          </div>

        <?php } // END - foreach $categories ?>

      </div> <!-- END Column MAIN -->

    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>
