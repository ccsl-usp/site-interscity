<?php 

// Not needed if the plugin is installed
//require_once( __DIR__ . '/functions/custom-fields.php');
require_once( __DIR__ . '/functions/shortcodes.php');
require_once( __DIR__ . '/functions/utilities.php');

//\( REST
//\( ––––––––––––––––––––––––––––––––––––––––––––––––––

require_once( __DIR__ . '/functions/rest.php');

//\( Menu
//\( ––––––––––––––––––––––––––––––––––––––––––––––––––

function register_my_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );


//\( Custom Get
//\( ––––––––––––––––––––––––––––––––––––––––––––––––––

require_once( __DIR__ . '/functions/get-publications.php');
require_once( __DIR__ . '/functions/get-software.php');

require_once( __DIR__ . '/functions/get-publications--home.php');
require_once( __DIR__ . '/functions/get-software--home.php');
require_once( __DIR__ . '/functions/get-events--home.php');
require_once( __DIR__ . '/functions/get-newsletters--home.php');
require_once( __DIR__ . '/functions/get-press--home.php');
require_once( __DIR__ . '/functions/get-news--home.php');


//\( Pagination
//\( ––––––––––––––––––––––––––––––––––––––––––––––––––

function inct_pagination()
{
  global $wp_query;
  $big = 999999999;
  $pages = paginate_links(array(
      'base' => str_replace($big, '%#%', get_pagenum_link($big)),
      'format' => '?paged=%#%',
      'current' => max(1, get_query_var('paged')),
      'total' => $wp_query->max_num_pages,
      'prev_next' => false,
      'type'  => 'array',
      'prev_next'   => true,
      'prev_text'    => __( 'Prev', 'text-domain' ),
      'next_text'    => __( 'Next', 'text-domain'),
  ));
  $output = '';

  if ( is_array( $pages ) ) {
    $paged = ( get_query_var('paged') == 0 ) ? 1 : get_query_var( 'paged' );

    $output .=  '<ul class="pagination">';
    foreach ( $pages as $page ) {
        $output .= "<li class='page-item'>$page</li>";
    }
    $output .= '</ul>';

    // Create an instance of DOMDocument 
    $dom = new \DOMDocument();

    // Populate $dom with $output, making sure to handle UTF-8, otherwise
    // problems will occur with UTF-8 characters.
    $dom->loadHTML( mb_convert_encoding( $output, 'HTML-ENTITIES', 'UTF-8' ) );

    // Create an instance of DOMXpath and all elements with the class 'page-numbers' 
    $xpath = new \DOMXpath( $dom );

    // http://stackoverflow.com/a/26126336/3059883
    $page_numbers = $xpath->query( "//*[contains(concat(' ', normalize-space(@class), ' '), ' page-numbers ')]" );

    // Iterate over the $page_numbers node...
    foreach ( $page_numbers as $page_numbers_item ) {

        // Add class="mynewclass" to the <li> when its child contains the current item.
        $page_numbers_item_classes = explode( ' ', $page_numbers_item->attributes->item(0)->value );
        if ( in_array( 'current', $page_numbers_item_classes ) ) {          
            $list_item_attr_class = $dom->createAttribute( 'class' );
            $list_item_attr_class->value = 'page-item active';
            $page_numbers_item->parentNode->appendChild( $list_item_attr_class );
        }

        // Replace the class 'current' with 'active'
        $page_numbers_item->attributes->item(0)->value = str_replace( 
                        'current',
                        'active',
                        $page_numbers_item->attributes->item(0)->value );

        // Replace the class 'page-numbers' with 'page-link'
        $page_numbers_item->attributes->item(0)->value = str_replace( 
                        'page-numbers',
                        '',
                        $page_numbers_item->attributes->item(0)->value );
    }

    // Save the updated HTML and output it.
    $output = $dom->saveHTML();
  }

  echo $output;
}


//\( Wordpress Hacks
//\( ––––––––––––––––––––––––––––––––––––––––––––––––––

function my_myme_types($mime_types){
    $mime_types['bib'] = 'bib'; //Adding bib extension
    return $mime_types;
}

add_filter('upload_mimes', 'my_myme_types', 1, 1);

// Better excerpts, with active links etc; taken almost
// verbatim from
// https://wordpress.stackexchange.com/questions/141125/allow-html-in-excerpt/141136#141136
function wpse_allowedtags() {
    // Add custom tags to this string
        return '<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>';
    }

if ( ! function_exists( 'wpse_custom_wp_trim_excerpt' ) ) :

    function wpse_custom_wp_trim_excerpt($wpse_excerpt) {
    $raw_excerpt = $wpse_excerpt;
        if ( '' == $wpse_excerpt ) {

            $wpse_excerpt = get_the_content('');
            $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
            $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
            $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
            $wpse_excerpt = strip_tags($wpse_excerpt, wpse_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

            //Set the excerpt word count and only break after sentence is complete.
                $excerpt_word_count = 75;
                $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count);
                $tokens = array();
                $excerptOutput = '';
                $count = 0;

                // Divide the string into tokens; HTML tags, or words, followed by any whitespace
                preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

                foreach ($tokens[0] as $token) {

                    if ($count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) {
                    // Limit reached, continue until , ; ? . or ! occur at the end
                        $excerptOutput .= trim($token);
                        break;
                    }

                    // Add words to complete sentence
                    $count++;

                    // Append what's left of the token
                    $excerptOutput .= $token;
                }

            $wpse_excerpt = trim(force_balance_tags($excerptOutput));

            return $wpse_excerpt;

        }
        return apply_filters('wpse_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
    }

endif;

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'wpse_custom_wp_trim_excerpt');

// captions create a div that, by default, have the nominal width of the
// image. Since we are using "width:100%" to resize them, let's remove
// this fixed width from the div. Copied from
// https://wordpress.stackexchange.com/a/310339
function disable_img_caption_shortcode_width($width, $atts, $content)
{
    return 0;
}

add_filter('img_caption_shortcode_width', 'disable_img_caption_shortcode_width', 10, 3);

?>
