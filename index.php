<?php get_header(); ?>

  <div class="container grid-base">
    <?php get_template_part('parts/menu', 'mobile'); ?>

    <div class="columns columns-main">
      
      <!-- Column MENU -->
      <?php get_template_part('parts/menu', 'main'); ?>
      
      <!-- Column MAIN -->
      <div class="column">
          
        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
          <h1 class="title-single"><?php the_title(); ?></h1>
          <?php the_content(); ?>
        <?php endwhile; ?>

        <?php else: ?>
          <h3>Sorry, nothing to display.</h3>
        <?php endif; ?>
        
      </div> <!-- END Column MAIN -->

    </div> <!-- END .columns.columns-main -->
  </div>

<?php get_footer(); ?>
