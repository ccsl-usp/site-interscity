$(document).ready(function() {

    // Slick Slider
    $(".slicker").slick({
        autoplay: true,
        autoplaySpeed: 2000,
    });

    // Menu Fixed when scroll
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 100) {
            $('header.navbar').addClass('is-fixed');
        } else {
            $('header.navbar').removeClass('is-fixed');
        }
    });


// .sub-card
    $('.sub-card .card-footer').click(function() {
        $(this).siblings('.card-body').slideToggle('slow');
        $(this).find('.icon').toggleClass('icon-arrow-up');
    })

// .card-list
    $('.card-list .card-header').click(function(e) {
        $(this).parent('.card-list').stop().toggleClass('is-open');
        $(this).siblings('.card-body').stop().slideToggle('fast');
        $(this).siblings('.icon').stop().toggleClass('icon-arrow-up');
        e.stopImmediatePropagation();
        e.preventDefault();
    })

//  make links within a .card-list work
    $('.card-list .card-header a').click(function(e) {
        e.stopPropagation();
    })

    $('.card-list .icon').click(function(e) {
        $(this).parent('.card-list').stop().toggleClass('is-open');
        $(this).siblings('.card-body').stop().slideToggle('fast');
        $(this).stop().toggleClass('icon-arrow-up');
        e.stopImmediatePropagation();
        e.preventDefault();
    })

});
